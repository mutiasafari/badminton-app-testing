import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'sample.GlobalFunction.openHome'()

//WebUI.click(findTestObject('Page_Home/a_Lets Count'))
//
//WebUI.verifyElementPresent(findTestObject('Page_Login/div_Badminton Apps v1 Please loginEmailEmail  is required PasswordPassword is required Login'), 
//    30)

CustomKeywords.'sample.Login.login'('admin@admin.com', 'password')

//WebUI.setText(findTestObject('Page_Login/input_Email_email'), GlobalVariable.username)
//
//WebUI.setText(findTestObject('Page_Login/input_Password_pass'), GlobalVariable.password)
//
//WebUI.click(findTestObject('Page_Login/button_Login'))
WebUI.verifyTextPresent('Selamat datang, Admin!', false)

CustomKeywords.'sample.GlobalFunction.closeBrowser'()

