<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suit Badminton App</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>734a5c61-eef4-43e2-a2d1-3cbf1da3aabd</testSuiteGuid>
   <testCaseLink>
      <guid>54098293-9a6c-489c-a59c-66caade2ee09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/B. Login/B.01 verifikasi login dengan data valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f645616e-b842-4f2a-a291-d31af26618c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/B. Login/B.02 verifikasi login dengan data invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e04e2a7-a77f-4486-b30b-c0ef7be35c95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/B. Login/B.03 verifikasi login dengan data kosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1ea4767-7076-4189-a380-1b90f794062b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A. Absensi/A.01 verifikasi tambah absensi dengan data valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84544362-91ea-40d2-9cc2-211adc7564b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A. Absensi/A.02 verifikasi absensi dengan data kosong</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
